<?php
namespace App\Controllers;
require_once('../models/Autor.php');
use \App\Models\Autor;

class AutorController  
{
    public function __construct()
    {
        //Estamos en el constructor
    }
    function index()
    {
        $autores = Autor::all();

        include('../../views/autor/index.php');
    }
    public function show($arguments)
    {
        $id = $arguments[0];
        echo "Mostrar el Autor $id";
        $autor = Autor::find($id);
        //generar la vista
        include('../../views/autor/show.php');
    }
    public function delete($arguments)
    {
        $id = $arguments[0];
        //enfoque 1:
        // $user = User::find($id);
        // $user->delete();

        // //enfoque 2
        Autor::destroy($id);

        //siempre redireccionar:
        header('Location: /autor');
        echo "Borrar del autor $id";        
    }

    public function create()
    {
        // echo "en create";
        include('../../views/autor/create.php');
    }

    public function store()
    {
        //crear objeto
        $autor = new Autor;
        $autor->nombre = $_POST['nombre'];
        $autor->apellidos = $_POST['apellidos'];
        $autor->insert();
        
        // "INSERT ...."
        // "UPDATE ...."
        //redirigir a la lista
        header('Location: /autor/index');
    }

    public function edit($arguments)
    {
        $id = $arguments[0];
        //buscar datos
        $autor = Autor::find($id);
        //mostrar vista
        //FALLOOOOO
        include('../../views/autor/edit.php');        
    }

    public function update($arguments)
    {
        $id = $arguments[0];
        //crear objeto
        $autor = Autor::find($id);
        $autor->nombre = $_POST['nombre'];
        $autor->apellido = $_POST['apellido'];

        $autor->save();
        
        // "INSERT ...."
        // "UPDATE ...."
        //redirigir a la lista
        header('Location: /autor/index');

    }
}




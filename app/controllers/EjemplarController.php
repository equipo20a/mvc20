<?php
namespace App\Controllers;

require_once('../app/models/Ejemplar.php');
use \App\Models\Ejemplar;

class UserController  
{
    public function __construct()
    {
        // echo "en UserController<br>";
    }

    public function index()
    {
        // echo "En método index<br>";

        //buscar la lista de usuarios
        $users = Ejemplar::all(); //arriba pongo use ...
        //generar la vista
        include('../../views/ejemplar/index.php');
    }
    
    public function show($arguments)
    {
        $id = $arguments[0];
        echo "Mostrar el ejemplar $id";        
        $ejemplar = Ejemplar::find($id);
        //generar la vista
        include('../../views/ejemplar/show.php');
    }
    
    public function delete($arguments)
    {
        $id = $arguments[0];
        //enfoque 1:
        // $user = User::find($id);
        // $user->delete();

        // //enfoque 2
        Ejemplar::destroy($id);

        //siempre redireccionar:
        header('Location: /ejemplar');
        echo "Borrar el ejemplar $id";        
    }

    public function create()
    {
        // echo "en create";
        include('../../views/ejemplar/create.php');
    }

    public function store()
    {
        //`libro_id`, `fecha_compra`, `disponible`, `ubicacion_id`
        //crear objeto
        $ejemplar = new Ejemplar;
        $ejemplar->libro_id = $_POST['libro_id'];
        $ejemplar->fecha_compra = $_POST['fecha_compra'];
        $ejemplar->disponible = $_POST['disponible'];
        $ejemplar->ubicacion_id = $_POST['ubicacion_id'];
        $ejemplar->insert();
        
        // "INSERT ...."
        // "UPDATE ...."
        //redirigir a la lista
        header('Location: /ejemplar/index');
    }

    public function edit($arguments)
    {
        $id = $arguments[0];
        //buscar datos
        $ejemplar = Ejemplar::find($id);
        //mostrar vista
        include('../../views/ejemplar/edit.php');        
    }

    public function update($arguments)
    {
        $id = $arguments[0];
        //crear objeto
        $ejemplar = Ejemplar::find($id);
        $ejemplar->libro_id = $_POST['libro_id'];
        $ejemplar->fecha_compra = $_POST['fecha_compra'];
        $ejemplar->disponible = $_POST['disponible'];
        $ejemplar->ubicacion_id = $_POST['ubicacion_id'];
        $ejemplar->save();
        
        // "INSERT ...."
        // "UPDATE ...."
        //redirigir a la lista
        header('Location: /ejemplar/index');

    }
}

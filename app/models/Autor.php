<?php
namespace App\Models;
require_once '../core/Model.php'; # preparo el acceso a otro fichero
use PDO;
use Core\Model; # sigo preparando mediante use.
class Autor  extends Model
{
    
    public function __construct()
    {
        //el constructor
    }

    public static function all()
    {
        $db = Autor::db();
        $statement = $db->query('SELECT * FROM autor');
        $autores = $statement->fetchAll(PDO::FETCH_CLASS, Autor::class);

        return $autores;        
    }
    public static function find($id)
    {
        $db = Autor::db();

        $statement = $db->prepare('SELECT * FROM autor WHERE id=:id');
        $statement->execute(array(':id' => $id));        
        $statement->setFetchMode(PDO::FETCH_CLASS, Autor::class);
        $autor = $statement->fetch(PDO::FETCH_CLASS);
        return $autor;
    }
    public function insert()
    {
        $db = Autor::db();
        $statement = $db->prepare('INSERT INTO autor(`nombre`, `apellidos`) VALUES(:nombre, :apellidos)');
        $data = [
            ':nombre' => $this->name,
            ':apellidos' => $this->surname
        ];
        return $statement->execute($data);
    }

    public function save()
    {
        $db = Autor::db();
        $statement = $db->prepare('UPDATE autor SET `nombre`=:nombre, `apellidos`=:apellidos WHERE id=:id');
        $data = [
            ':id' => $this->id,
            ':nombre' => $this->nombre,
            ':apellidos' => $this->apellidos
        ];
        return $statement->execute($data);
    }
    
    public function delete()
    {
        $db = Autor::db();
        $statement = $db->prepare('DELETE FROM autor WHERE id=:id');        
        return $statement->execute([':id' => $this->id]);        
    }

    public static function destroy($id)
    {
        $db = Autor::db();
        $statement = $db->prepare('DELETE FROM autor WHERE id=:id');        
        return $statement->execute([':id' => $id]);        
    }






}

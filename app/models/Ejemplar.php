<?php
namespace App\Models;

require_once '../core/Model.php'; # preparo el acceso a otro fichero

use PDO;
use Core\Model; # sigo preparando mediante use.

class Ejemplar extends Model
{
    public function __construct()
    {
        # code...
    }

    public static function all()
    {
        $db = Ejemplar::db();
        $statement = $db->query('SELECT * FROM ejemplar');
        $ejemplar = $statement->fetchAll(PDO::FETCH_CLASS, Ejemplar::class);

        return $ejemplar;        
    }

    public static function find($id)
    {
        $db = Ejemplar::db();

        $statement = $db->prepare('SELECT * FROM ejemplar WHERE id=:id');
        $statement->execute(array(':id' => $id));        
        $statement->setFetchMode(PDO::FETCH_CLASS, Ejemplar::class);
        $ejemplar = $statement->fetch(PDO::FETCH_CLASS);
        return $ejemplar;
    }

    public function insert()
    {
        $db = Ejemplar::db();
        $statement = $db->prepare('INSERT INTO ejemplar(`libro_id`, `fecha_compra`, `disponible`, `ubicacion_id`) VALUES(:libro_id, :fecha_compra, :disponible, :ubicacion_id)');
        $data = [
            ':libro_id' => $this->libro_id,
            ':fecha_compra' => $this->fecha_compra,
            ':disponible' => $this->disponible,
            ':ubicacion_id' => $this->ubicacion_id
        ];
        return $statement->execute($data);
    }

    public function save()
    {
        $db = Ejemplar::db();
        $statement = $db->prepare('UPDATE ejemplar SET `libro_id`=:libro_id, `fecha_compra`=:fecha_compra, `disponible`=:disponible, `ubicacion_id`=:ubicacion_id WHERE id=:id');
        $data = [
            ':id' => $this->id,
            ':libro_id' => $this->libro_id,
            ':fecha_compra' => $this->fecha_compra,
            ':disponible' => $this->disponible,
            ':ubicacion_id' => $this->ubicacion_id
        ];
        return $statement->execute($data);
    }
    
    public function delete()
    {
        $db = Ejemplar::db();
        $statement = $db->prepare('DELETE FROM ejemplar WHERE id=:id');        
        return $statement->execute([':id' => $this->id]);        
    }

    public static function destroy($id)
    {
        $db = Ejemplar::db();
        $statement = $db->prepare('DELETE FROM ejemplar WHERE id=:id');        
        return $statement->execute([':id' => $id]);        
    }
}

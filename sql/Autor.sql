CREATE DATABASE IF NOT EXISTS `mvc20A` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mvc20A`;

DROP TABLE IF EXISTS `autor`;
CREATE TABLE `autor` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidos` varchar(50)
) 

INSERT INTO `autor` (`id`, `nombre`, `apellidos`) VALUES
(1, 'Miguel', 'Quintana'),
(2, 'Ana', 'Rivas'),
(3, 'David', 'Castillon'),
(4, 'Miguel', 'Valenzuela'),
(5, 'Jose', 'Ruiz'),
(6, 'Maria', 'Gomez'),
(7, 'Mar', 'Mejia')
;

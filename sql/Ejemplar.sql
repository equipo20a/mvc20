CREATE DATABASE IF NOT EXISTS `mvc20A` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mvc20A`;

DROP TABLE IF EXISTS `ejemplar`;
CREATE TABLE `ejemplar` (
    `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `libro_id` int (50),
    `fecha_compra` varchar(50),
    `disponible` varchar(50),
    `ubicacion_id` varchar(50)
    
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `ejemplar` (`id`, `libro_id`, `fecha_compra`, `disponible`, `ubicacion_id`) VALUES
(1, '564-84-685-585-8', '2000-04-12', 'Si', 1),
(2, '123-84-4569-1582', '1999-12-01', 'No', 2),
(3, '4785-59544-28241', '2020-01-20', 'Si', 3),
(4, '8752-52582-25854', '2001-08-18', 'Si', 4),
(5, '8585-25454-54815', '2001-01-14', 'Si', 5),
(6, '5456-12344-44695', '2001-08-18', 'No', 6),
(7, '9756-22697-52524', '2001-02-17', 'Si', 7)
;

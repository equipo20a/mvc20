<?php include('../views/parts/head.php'); ?>
<?php include('../views/parts/header.php'); ?>
<!-- Begin page content -->
<main role="main" class="container">    
    <h1>Crear autor</h1>

    <form class="form" action="/autor/store" method="POST">

    <div class="form-group">
        <label for="nombre">Nombre:</label>
        <input class="form-control" type="text" name="nombre"> 
    </div>

    <div class="form-group">
        <label for="apellidos">Apellidos:</label>
        <input class="form-control" type="text" name="apellidos"> 
    </div>

    <div class="form-group">
        <input class="form-control" type="submit" value="Guardar"> 
    </div>

    </form>
</main>

<?php include('../views/parts/footer.php'); ?>
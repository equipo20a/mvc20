<?php include('../views/parts/head.php'); ?>
<?php include('../views/parts/header.php'); ?>
<!-- Begin page content -->
<main role="main" class="container">    
    <h1>Edición de Autor</h1>

    <form class="form" action="/autor/update/<?= $autor->id ?>" method="POST">

    <div class="form-group">
        <label for="nombre">Nombre:</label>
        <input class="form-control" type="text" value="<?= $autor->nombre ?>" name="nombre"> 
    </div>

    <div class="form-group">
        <label for="apellidos">Apellidos:</label>
        <input class="form-control" type="text" value="<?= $autor->apellidos ?>" name="apellidos"> 
    </div>


    <div class="form-group">
        <input class="form-control" type="submit" value="Guardar"> 
    </div>

    </form>
</main>

<?php include('../views/parts/footer.php'); ?>

<?php include('../views/parts/head.php'); ?>
<?php include('../views/parts/header.php'); ?>
<!-- Begin page content -->
<main role="main" class="container">
  <h1>Lista de Autores  
      <a class="btn btn-primary float-right" href="/autor/create">Nuevo</a>
  </h1>
  <table class="table table-striped">
        <thead>
            <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th></th>
            <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($autores as $autor) {?>
                <tr>
                <td><?= $autor->nombre ?></td>
                <td><?= $autor->apellidos ?></td>
                <td><a class="btn btn-primary btn-sm" href="/autor/show/<?= $autor->id ?>">  Ver </a></td>
                <td><a class="btn btn-primary btn-sm" href="/autor/edit/<?= $autor->id ?>">  Editar </a></td>
                <td><a class="btn btn-primary btn-sm" href="/autor/delete/<?= $autor->id ?>">  Borrar </a></td>
                </tr>
            <?php } ?>            
        </tbody>
    </table>
</main>

<?php include('../views/parts/footer.php'); ?>

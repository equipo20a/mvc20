<?php include('../views/parts/head.php'); ?>
<?php include('../views/parts/header.php'); ?>
<!-- Begin page content -->
<main role="main" class="container">    
    <h1>Detalle de los autores</h1>
    <div class="card">
        <div class="card-header">
            Autor número <?= $autor->id ?>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">Nombre: <?= $autor->nombre ?></li>
            <li class="list-group-item">Apellidos: <?= $autor->apellidos ?></li>
        </ul>
  </div>
</main>

<?php include('../views/parts/footer.php'); ?>
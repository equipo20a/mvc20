<?php include('../views/parts/head.php'); ?>
<?php include('../views/parts/header.php'); ?>
<!-- Begin page content-->
<main role="main" class="container">    
    <h1>Alta de ejemplar</h1>

    <form class="form" action="/ejemplar/store" method="POST">

    <div class="form-group">
        <label for="libro_id">ID del libro:</label>
        <input class="form-control" type="text" name="libro_id"> 
    </div>

    <div class="form-group">
        <label for="fecha_compra">Fecha de la compra:</label>
        <input class="form-control" type="date" name="fecha_compra"> 
    </div>

    <div class="form-group">
        <label for="disponible">Disponible:</label>
        <input class="form-control" type="text" name="disponible"> 
    </div>

    <div class="form-group">
        <label for="ubicacion_id">ID de la ubicacion:</label>
        <input class="form-control" type="text" name="ubicacion_id"> 
    </div>

    <div class="form-group">
        <input class="form-control" type="submit" value="Guardar"> 
    </div>

    </form>
</main>

<?php include('../views/parts/footer.php'); ?>

<?php include('../views/parts/head.php'); ?>
<?php include('../views/parts/header.php'); ?>
<!-- Begin page content -->
<main role="main" class="container">    
    <h1>Edición de Ejemplares</h1>

    <form class="form" action="/ejemplar/update/<?= $ejemplar->id ?>" method="POST">

    <div class="form-group">
        <label for="libro_id">ID del libro:</label>
        <input class="form-control" type="text" value="<?= $ejemplar->libro_id ?>" name="libro_id"> 
    </div>

    <div class="form-group">
        <label for="fecha_compra">Fecha de la compra:</label>
        <input class="form-control" type="data" value="<?= $ejemplar->fecha_compra ?>" name="fecha_compra"> 
    </div>

    <div class="form-group">
        <label for="disponible">Disponible:</label>
        <input class="form-control" type="text" value="<?= $ejemplar->disponible ?>" name="disponible"> 
    </div>

    <div class="form-group">
        <label for="ubicacion_id">ID de la ubicacion:</label>
        <input class="form-control" type="text" value="<?= $ejemplar->ubicacion_id ?>" name="ubicacion_id"> 
    </div>

    <div class="form-group">
        <input class="form-control" type="submit" value="Guardar"> 
    </div>

    </form>
</main>

<?php include('../views/parts/footer.php'); ?>
<?php include('../views/parts/head.php'); ?>
<?php include('../views/parts/header.php'); ?>
<!-- Begin page content -->
<main role="main" class="container">
  <h1>Lista de ejemplares  
      <a class="btn btn-primary float-right" href="/ejemplar/create">Nuevo</a>
  </h1>
  <table class="table table-striped">
        <thead>
            <tr>
            <th>ID del libro</th>
            <th>Fecha de la compra</th>
            <th>Disponible</th>
            <th>ID de la ubicacion</th>
            <th></th>
            <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($ejemplar as $mejemplar) {?>
                <tr>
                <td><?= $mejemplar->libro_id ?></td>
                <td><?= $mejemplar->fecha_compra ?></td>
                <td><?= $mejemplar->disponible ?></td>
                <td><?= $mejemplar->ubicacion_id ?></td>
                <td><a class="btn btn-primary btn-sm" href="/ejemplar/show/<?= $mejemplar->id ?>">  Ver </a></td>
                <td><a class="btn btn-primary btn-sm" href="/ejemplar/edit/<?= $mejemplar->id ?>">  Editar </a></td>
                <td><a class="btn btn-primary btn-sm" href="/ejemplar/delete/<?= $mejemplar->id ?>">  Borrar </a></td>
                </tr>
            <?php } ?>            
        </tbody>
    </table>
</main>

<?php include('../views/parts/footer.php'); ?>
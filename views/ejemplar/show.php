<?php include('../views/parts/head.php'); ?>
<?php include('../views/parts/header.php'); ?>
<!-- Begin page content `libro_id`, `fecha_compra`, `disponible`, `ubicacion_id`-->
<main role="main" class="container">    
    <h1>Detalle de los ejemplares</h1>
    <div class="card">
        <div class="card-header">
            Ejemplar número <?= $ejemplar->id ?>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">ID del libro: <?= $ejemplar->libro_id ?></li>
            <li class="list-group-item">Fecha de la compra: <?= $ejemplar->fecha_compra ?></li>
            <li class="list-group-item">Disponible: <?= $ejemplar->disponible ?></li>
            <li class="list-group-item">ID de la ubicacion: <?= $ejemplar->ubicacion_id ?></li>
        </ul>
  </div>    
</main>

<?php include('../views/parts/footer.php'); ?>